/*  ============================================================================================================

    MrDIY Backsplash Notifier is Visual and Audible notifier that is embedded in a kitchen backsplash.


    + MQTT COMMANDS:

        - Start a timer              MQTT topic: "cmnd/backsplash/timer"
        - Cooktop status             MQTT topic: "cmnd/backsplash/corner"  ( 1=off, 2=on, 3=blink - color: RED)
        - Audio beep                 MQTT topic: "cmnd/backsplash/beep"  ( 1=1 beep, 2= 2 beeps ...etc max: 15 beeps)
        - Power management           MQTT topic: "cmnd/backsplash/power"  ( 0=turn all lights off, 2=reboot)

    + STATUS UPDATES:

      - The notifier sends status updates on MQTT topic: "stat/backsplash/status" with these values:

                  "LWT"                 values: online and offline on stat/backsplash/LWT
                  "corner"              cooktop light updated
                  "timer started"       timer started
                  "timer ended"         timer ended
                  "idle"                device started
                  "error"               something went wrong with the last command sent

      - At boot, the device will beep then it is connected to Wifi and MQTT
      - A web server is served on port 80 ( visit http://the device-ip-address/


    + SETUP:

       1 - Update the Wifi and MQTT values in: WIFI_NAME, WIFI_PASSWORD, MQTT_SERVER & MQTT_PORT
       2 - Delete this line: "#define MRDIY" in the code
       3 - Connect the LED strip to pin GPIO2
       4 - Connect the Buzzer to pin GPI14
       5- Connect buttons to pins GPIO4, GPIO5 & GPIO12

      Check the schematic and PCB design for this project in my Git repo.

    + DEPENDENCIES:

      - ESP8266
      - ESP8266WebServer
      - ArduinoOTA
      - WS2812FX
      - ArduinoJson
      - PubSubClient

    Many thanks to all the authors and contributors to the above libraries - amazing job!

    For more info, please watch my video at https://youtu.be/RUTScf3DY2Y
    MrDIY.ca

  ============================================================================================================== */

#define DEBUG_FLAG              // uncomment to enable debug mode & Serial output
//#define MRDIY
#define WEBSERVER

#include <ESP8266WiFi.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <WS2812FX.h>
#include <ArduinoJson.h>
#include <AceButton.h>

using namespace ace_button;

#ifdef WEBSERVER
#include <ESP8266WebServer.h>
extern const char index_html[];
extern const char main_js[];
#endif

#ifdef MRDIY
#include "config_MrDIY.h"
#endif

/* ################# CHANGE ME ######################## */

#ifndef MRDIY
#define WIFI_NAME             "username"
#define WIFI_PASSWORD         "password"
#define MQTT_SERVER           "x.x.x.x"
#define MQTT_PORT             1883
#endif

/* ##################################################### */

WiFiClient espClient;
PubSubClient client(espClient);

#define LED_PIN                 2
#define BUZZER_PIN              14
#define ANALOG_PIN              A0
#define BUTTON_1_PIN            4
#define BUTTON_2_PIN            5
#define BUTTON_3_PIN            12

#define LED_COUNT               29
#define DEFAULT_COLOR           BLACK
#define DEFAULT_BRIGHTNESS      30
#define DEFAULT_SPEED           1000
#define DEFAULT_MODE            FX_MODE_STATIC

#define SEG_1_START             0
#define SEG_1_END               0
#define SEG_2_START             1
#define SEG_2_END               27
#define SEG_3_START             28
#define SEG_3_END               28
#define TIMER_NUMBER_OF_BEEPS   7

bool button_1_status = HIGH, button_2_status = HIGH, button_3_status = HIGH;
bool cooktop_status = 0, circulation_status = 0;

int last_brightness             = DEFAULT_BRIGHTNESS;
unsigned long cooking_timer     = 0;
unsigned int timer_last_trigger = 0, timer_minutes_left = 0, last_timer_minutes_left_sent = 0;
unsigned long lastDebounceTime  = 0;                  // the last time the output pin was toggled
unsigned long debounceDelay     = 500;               // the debounce time; increase if the output flickers

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

WS2812FX                        ws2812fx = WS2812FX(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

#ifdef WEBSERVER
ESP8266WebServer                server(80);
#endif
AceButton                       upButton(BUTTON_1_PIN);
AceButton                       downButton(BUTTON_2_PIN);
AceButton                       resetButton(BUTTON_3_PIN);
void handleButtons(AceButton* button, uint8_t eventType, uint8_t /*buttonState*/);


/* ################################## Setup ############################################# */

void setup() {

  pinMode(BUTTON_1_PIN, INPUT_PULLUP);
  pinMode(BUTTON_2_PIN, INPUT_PULLUP);
  pinMode(BUTTON_3_PIN, INPUT_PULLUP);

#ifdef DEBUG_FLAG
  Serial.begin(115200);
#endif

#ifdef DEBUG_FLAG
  Serial.println();
  Serial.println(F("=================================================================="));
  Serial.println();
  Serial.println(F("  MrDIY Backsplash"));
  Serial.println();
  Serial.println(F("=================================================================="));
  Serial.println();
#endif

  setup_wifi();
  client.setServer(MQTT_SERVER, 1883);
  client.setCallback(callback);
  if (!client.connected()) reconnect();

  ws2812fx.init();
  ws2812fx.setMode(DEFAULT_MODE);
  ws2812fx.setColor(DEFAULT_COLOR);
  ws2812fx.setSpeed(DEFAULT_SPEED);
  ws2812fx.setBrightness(DEFAULT_BRIGHTNESS);
  ws2812fx.setIdleSegment(0, SEG_1_START, SEG_3_END, DEFAULT_MODE, DEFAULT_COLOR, 1000, NO_OPTIONS);
  ws2812fx.setIdleSegment(1, SEG_1_START, SEG_1_END, DEFAULT_MODE, DEFAULT_COLOR, 1000, NO_OPTIONS);
  ws2812fx.setIdleSegment(2, SEG_2_START, SEG_2_END, DEFAULT_MODE, DEFAULT_COLOR, 1000, NO_OPTIONS);
  ws2812fx.setIdleSegment(3, SEG_3_START, SEG_3_END, DEFAULT_MODE, DEFAULT_COLOR, 1000, NO_OPTIONS);
  ws2812fx.start();

#ifdef WEBSERVER
  server.on("/", srv_handle_index_html);
  server.on("/main.js", srv_handle_main_js);
  server.on("/timer", srv_handle_timer);
  server.on("/add_a_minute", srv_handle_add);
  server.on("/sub_a_minute", srv_handle_sub);
  server.on("/get_cooktop_status", get_cooktop_status);
  server.on("/get_circulation_status", get_circulation_status);
  server.on("/set", srv_handle_set);
  server.on("/status", srv_handle_status);
  server.onNotFound(srv_handle_not_found);
  server.begin();
#endif

  ArduinoOTA.setHostname("MrDIY-Backsplash");
  ArduinoOTA.begin();

  ButtonConfig* buttonConfig = ButtonConfig::getSystemButtonConfig();
  buttonConfig->setEventHandler(handleButtons);
  buttonConfig->setFeature(ButtonConfig::kFeatureLongPress);

  beeper(100, 500);
  beeper(100, 500);
}



/* ##################################### Loop ############################################# */

void loop() {

  if (!client.connected()) reconnect();
  client.loop();
#ifdef WEBSERVER
  server.handleClient();
#endif
  ws2812fx.service();
  ArduinoOTA.handle();
  updateBrightness();
  readButtons();
  updateTimer();
}

void readButtons() {

  upButton.check();
  downButton.check();
  resetButton.check();
}
/* ############################ WifiManager ############################################# */


void setup_wifi() {

  delay(10);

#ifdef DEBUG_FLAG
  Serial.println();
  Serial.print("Connecting to ");
  Serial.print(WIFI_NAME);
  Serial.print(" ");
#endif
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_NAME, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
#ifdef DEBUG_FLAG
  Serial.println("");
  Serial.print("IP ");
  Serial.println(WiFi.localIP());
#endif

}

void reconnect() {

  while (!client.connected()) {
#ifdef DEBUG_FLAG
    Serial.print("MQTT ");
#endif
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str(), "", "", "stat/backsplash/LWT", 0, false, "offline")) {
#ifdef DEBUG_FLAG
      Serial.println("connected");
#endif
      client.publish("stat/backsplash/status", "idle");
      client.publish("stat/backsplash/LWT", "online");

      client.subscribe("cmnd/backsplash/corner");
      client.subscribe("cmnd/backsplash/light");
      client.subscribe("cmnd/backsplash/beep");
      client.subscribe("cmnd/backsplash/timer");
      client.subscribe("cmnd/backsplash/power");
    } else {
#ifdef DEBUG_FLAG
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
#endif
      delay(5000);
    }
  }
}

/* ################################## MQTT ############################################### */


void callback(char* topic, byte * payload, unsigned int length) {

#ifdef DEBUG_FLAG
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
#endif
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
#ifdef DEBUG_FLAG
  Serial.println();
#endif
  String topicStr(topic);

  // BEEP -------------------------------------------------------------------
  if ( strcmp(topic, "cmnd/backsplash/beep") == 0 ) {
#ifdef DEBUG_FLAG
    Serial.println("Message arrived [beep]");
#endif
    payload[length] = '\0';
    int count = atoi((char *)payload);
    if ( count > 0 && count <= 20) {
      client.publish("stat/backsplash/beeper", "1");
      while (count > 0) {
        beeper(100, 200);
        beeper(100, 200);
        beeper(100, 200);
        beeper(100, 600);
        count = count - 1;
      }
      client.publish("stat/backsplash/beeper", "0");
      client.publish("cmnd/backsplash/status", "beep");
    } else {
      client.publish("cmnd/backsplash/status", "error: beep");
    }
  }

  // CORNER -------------------------------------------------------------------

  if ( strcmp(topic, "cmnd/backsplash/corner") == 0 ) {

#ifdef DEBUG_FLAG
    Serial.print("Message arrived [corner] ");
#endif
    payload[length] = '\0';
    int rgb = atoi((char *)payload);

    if ( rgb == 1 ) {

      ws2812fx.setSegment(1, SEG_1_START, SEG_1_END, DEFAULT_MODE, BLACK, 1000, false);
      ws2812fx.setSegment(3, SEG_3_START, SEG_3_END, DEFAULT_MODE, BLACK, 1000, false);
      cooktop_status = circulation_status = 0;
      client.publish("stat/backsplash/status", "corner: off");

    } else if ( rgb == 2 ) {

      ws2812fx.setSegment(1, SEG_1_START, SEG_1_END, DEFAULT_MODE, RED, 1000, false);
      ws2812fx.setSegment(3, SEG_3_START, SEG_3_END, DEFAULT_MODE, RED, 1000, false);
      cooktop_status = 1;;
      client.publish("stat/backsplash/status", "corner: red");

    } else if (rgb  == 3) {

      ws2812fx.setSegment(1, SEG_1_START, SEG_1_END, FX_MODE_BLINK, RED, 1000, false);
      ws2812fx.setSegment(3, SEG_3_START, SEG_3_END, FX_MODE_BLINK, RED, 1000, false);
      circulation_status = 1;
      client.publish("stat/backsplash/status", "corner: red blink");
    } else {
      client.publish("stat/backsplash/status", "error: corner");
    }
  }

  // LIGHT -------------------------------------------------------------------

  if ( strcmp(topic, "cmnd/backsplash/light") == 0 ) {
#ifdef DEBUG_FLAG
    Serial.print("Message arrived [light] ");
#endif
    char* payloadStr = (char*)payload;
    uint32_t rgb = 0;
    for (int i = 0; i < strlen(payloadStr) - 1; i++) {
      rgb = (rgb * 16) + x2b(payloadStr[i]);
    }
#ifdef DEBUG_FLAG
    Serial.println(rgb, HEX);
#endif
    if (rgb >= 0x000000 && rgb <= 0xFFFFFF) {
      ws2812fx.setIdleSegment(2, SEG_2_START, SEG_2_END, DEFAULT_MODE, DEFAULT_COLOR, 1000, NO_OPTIONS);
      ws2812fx.setSegment(2, SEG_2_START, SEG_2_END, DEFAULT_MODE, rgb, 1000, false);
      client.publish("stat/backsplash/status", "light");
    } else {
      client.publish("stat/backsplash/status", "error: light");
    }
  }

  // TIMER -------------------------------------------------------------------

  if ( strcmp(topic, "cmnd/backsplash/timer") == 0 ) {
#ifdef DEBUG_FLAG
    Serial.println("Message arrived [timer]");
#endif
    payload[length] = '\0';
    int aNumber = atoi((char *)payload);
    if ( aNumber > 0 && aNumber < 60 * 6) {
      cooking_timer = millis() + aNumber * 1000 * 60;
      updateTimer();
      initiateTimer();
    } else {
      client.publish("stat/backsplash/status", "error: timer");
    }
  }

  // POWER -------------------------------------------------------------------

  if ( strcmp(topic, "cmnd/backsplash/power") == 0 ) {
#ifdef DEBUG_FLAG
    Serial.println("Message arrived [power]");
#endif
    payload[length] = '\0';
    int aNumber = atoi((char *)payload);
    if (aNumber == 0)  {
      ws2812fx.strip_off();
      client.publish("stat/backsplash/status", "turned off");
    }
    if (aNumber == 2) {
      ESP.restart();
      client.publish("stat/backsplash/status", "restarted");
    }
  }
}

/* ############################ Beeper  ############################################# */

void beeper(unsigned char duration, int delayms ) {

  tone(BUZZER_PIN, 1568, duration);
  delay(delayms);
  noTone(BUZZER_PIN);
}

/* ############################ Timer  ############################################# */

void updateTimer() {

  if (cooking_timer > millis()) {

    timer_minutes_left = floor((cooking_timer - millis()) / 60000 ); // results in minutes ( 1 LED = 1 minute )

    if ( timer_minutes_left != last_timer_minutes_left_sent ) {
      String s;
      char msg[50];
      last_timer_minutes_left_sent = timer_minutes_left;
      s = String(timer_minutes_left);
      s.toCharArray(msg, s.length() + 1);
      client.publish("stat/backsplash/timer", msg);
    }

    if (cooking_timer - millis() < 1000 * TIMER_NUMBER_OF_BEEPS) {
      client.publish("stat/backsplash/beeper", "1");
      int count = TIMER_NUMBER_OF_BEEPS;
      while (count > 0) {
        beeper(100, 200);
        beeper(100, 200);
        beeper(100, 200);
        beeper(100, 600);
        count = count - 1;
      }
      stopTimer();
      client.publish("stat/backsplash/beeper", "0");
    }
  }
}


void initiateTimer() {

  ws2812fx.setCustomMode(myTimerEffect);
  ws2812fx.setSegment(2, SEG_2_START, SEG_2_END , FX_MODE_CUSTOM, WHITE, 1000, false);
  client.publish("stat/backsplash/status", "timer");
}

void stopTimer() {

  cooking_timer = 0;
  timer_minutes_left = 0;
  ws2812fx.setSegment(2, SEG_2_START, SEG_2_END , DEFAULT_MODE, BLACK, 1000, false);
}

/* ############################ Light  ############################################# */

void updateBrightness() {

  // TODO - this code freezes the ESP
  /*
    float delt = last_brightness - analogRead(ANALOG_PIN) / 5;

    if ( delt > 30 || delt < -30 ) {
      last_brightness = analogRead(ANALOG_PIN) / 5;
      ws2812fx.setBrightness(last_brightness);
      //tone(BUZZER_PIN, 1568, 100);
    }
  */
}

uint16_t myCustomEffect(void) {

  WS2812FX::Segment* seg = ws2812fx.getSegment();
  for (uint16_t i = seg->start; i <= seg->stop; i++ ) {
    ws2812fx.setPixelColor(i, BLACK);
  }

  for (uint16_t i = seg->start; i <= seg->stop; i++ ) {
    if ( i <= timer_minutes_left + seg->start ) {
      ws2812fx.setPixelColor(i, WHITE);
      if ( i % 5 == 0) ws2812fx.setPixelColor(i, BLUE);
    }
  }
  return seg->speed;
}


uint16_t myTimerEffect(void) {

  // - - - - - - - x x x x x x   x x x x x x x x x - - - -
  WS2812FX::Segment* seg = ws2812fx.getSegment();

  for (uint16_t i = seg->start; i <= seg->stop; i++ ) {
    ws2812fx.setPixelColor(i, BLACK);
  }

  int midpoint = seg->start +  (seg->stop -  seg->start) / 2;

  int minutes_left = timer_minutes_left % 10;
  int ten_minutes_left = floor(timer_minutes_left / 10);

  if ( minutes_left == 9 ) {
    minutes_left = -1;
    ten_minutes_left++;
  }

  for (int i = midpoint + 1; i < midpoint + 1 + ten_minutes_left ; i++) {
    ws2812fx.setPixelColor( i, BLUE);
  }
  int fiver = 0 ;
  for (int i = midpoint - 1; i >= midpoint - 1 - minutes_left; i--) {
    if ( fiver++ == 4 ) ws2812fx.setPixelColor( i, PINK);
    else ws2812fx.setPixelColor( i, WHITE);
  }

  return seg->speed;
}

/* ############################ Buttons  ############################################# */

void handleButtons(AceButton* button, uint8_t eventType,  uint8_t /*buttonState*/) {
  uint8_t pin = button->getPin();

  if (pin == BUTTON_1_PIN) {
    switch (eventType) {
      // Interpret a Released event as a Pressed event, to distiguish it
      // from a LongPressed event.
      case AceButton::kEventPressed:
#ifdef DEBUG_FLAG
        Serial.println(F("Mode Button: Pressed"));
#endif
        if ( cooking_timer < millis() ) {
          cooking_timer = millis() + 1000 * 60; // start with a minute
          initiateTimer();
        } else cooking_timer = cooking_timer + 1000 * 60; // add an extra minute
        client.publish("stat/backsplash/button/1", "1");
        client.publish("stat/backsplash/button/1", "0");
        break;

      case AceButton::kEventLongPressed:
#ifdef DEBUG_FLAG
        Serial.println(F("Mode Button: Long Pressed"));
#endif
        if ( cooking_timer < millis() ) {
          cooking_timer = millis() + 1000 * 60; // start with a minute
          initiateTimer();
        } else cooking_timer = cooking_timer + 1000 * 60 * 4; // add an extra minute
        client.publish("stat/backsplash/button/1", "2");
        client.publish("stat/backsplash/button/1", "0");
        break;
    }
  }


  if (pin == BUTTON_2_PIN) {
    switch (eventType) {
      // Interpret a Released event as a Pressed event, to distiguish it
      // from a LongPressed event.
      case AceButton::kEventPressed:
#ifdef DEBUG_FLAG
        Serial.println(F("Mode Button: Pressed"));
#endif
        if ( cooking_timer > millis() + 1000 * 60 ) cooking_timer = cooking_timer - 1000 * 60;
        else  stopTimer();
        client.publish("stat/backsplash/button/2", "1");
        client.publish("stat/backsplash/button/2", "0");
        break;

      case AceButton::kEventLongPressed:
#ifdef DEBUG_FLAG
        Serial.println(F("Mode Button: Long Pressed"));
#endif
        if ( cooking_timer > millis() + 1000 * 60 * 4 ) cooking_timer = cooking_timer - 1000 * 60 * 4;
        else  stopTimer();
        client.publish("stat/backsplash/button/2", "2");
        client.publish("stat/backsplash/button/2", "0");
        break;
    }
  }

  if (pin == BUTTON_3_PIN) {
    switch (eventType) {
      // Interpret a Released event as a Pressed event, to distiguish it
      // from a LongPressed event.
      case AceButton::kEventPressed:
#ifdef DEBUG_FLAG
        Serial.println(F("Mode Button: Pressed"));
#endif
        stopTimer();
        ws2812fx.setSegment(2, SEG_2_START, SEG_2_END, DEFAULT_MODE, WHITE, 1000, false);
        client.publish("stat/backsplash/button/3", "1");
        client.publish("stat/backsplash/button/3", "0");
        break;

      case AceButton::kEventLongPressed:
#ifdef DEBUG_FLAG
        Serial.println(F("Mode Button: Long Pressed"));
#endif
        //ESP.restart();
        ws2812fx.setSegment(2, SEG_2_START, SEG_2_END, DEFAULT_MODE, BLACK, 1000, false);
        client.publish("stat/backsplash/button/3", "2");
        client.publish("stat/backsplash/button/3", "0");
        break;
    }
  }
}

/* ############################ Web ############################################# */
#ifdef WEBSERVER

void srv_handle_not_found() {

  server.send(404, "text/plain", "File Not Found");
}

void srv_handle_index_html() {

  server.send_P(200, "text/html", index_html);
}

void srv_handle_timer() {

  char str[10];
  int time_left = (cooking_timer - millis()) / 1000;
  itoa( time_left, str, 10 );
  if ( cooking_timer > millis() ) server.send_P(200, "text/html",  str );
  else server.send_P(200, "text/html",  "0" );
}

void srv_handle_main_js() {

  server.send_P(200, "application/javascript", main_js);
}

void srv_handle_add() {
  if ( cooking_timer < millis() ) {
    cooking_timer = millis() + 1000 * 60; // start with a minute
    initiateTimer();
  } else cooking_timer = cooking_timer + 1000 * 60; // add an extra minute
  server.send_P(200, "text/html", index_html);
}

void get_cooktop_status() {
  
  if(cooktop_status == 1) server.send_P(200, "text/html", "1");
  else server.send_P(200, "text/html", "0");
}

void get_circulation_status() {
  if(circulation_status == 1) server.send_P(200, "text/html", "1");
  else server.send_P(200, "text/html", "0");
}

void srv_handle_sub() {
  if ( cooking_timer > millis() + 1000 * 60 ) cooking_timer = cooking_timer - 1000 * 60;
  else  stopTimer();
  server.send_P(200, "text/html", index_html);
}

void srv_handle_set() {

  for (uint8_t i = 0; i < server.args(); i++) {
    if (server.argName(i) == "c") {
      uint32_t tmp = (uint32_t) strtol(server.arg(i).c_str(), NULL, 10);
      if (tmp >= 0x000000 && tmp <= 0xFFFFFF) {
        ws2812fx.setColor(tmp);
      }
    }
  }
  server.send(200, "text/plain", "OK");
}

void srv_handle_status() {

  DynamicJsonBuffer jsonBuffer(1000);
  JsonObject& root = jsonBuffer.createObject();

  root["pin"] = ws2812fx.getPin();
  root["numPixels"] = ws2812fx.numPixels();
  root["brightness"] = ws2812fx.getBrightness();
  root["numSegments"] = ws2812fx.getNumSegments();
  JsonArray& jsonSegments = root.createNestedArray("segments");

  WS2812FX::segment* segments = ws2812fx.getSegments();
  for (int i = 0; i < ws2812fx.getNumSegments(); i++) {
    WS2812FX::segment seg = segments[i];
    JsonObject& jsonSegment = jsonBuffer.createObject();
    jsonSegment["start"] = seg.start;
    jsonSegment["stop"] = seg.stop;
    jsonSegment["mode"] = seg.mode;
    jsonSegment["speed"] = seg.speed;
    jsonSegment["options"] = seg.options;
    JsonArray& jsonColors = jsonSegment.createNestedArray("colors");
    jsonColors.add(seg.colors[0]);
    jsonColors.add(seg.colors[1]);
    jsonColors.add(seg.colors[2]);
    jsonSegments.add(jsonSegment);
  }
  int bufferSize = root.measureLength() + 1;
  char *json = (char*)malloc(sizeof(char) * (bufferSize));
  root.printTo(json, sizeof(char) * bufferSize);
  server.send(200, "application/json", json);
  free(json);
}
#endif
/* ############################ Tools  ############################################# */

byte x2b( char c) {

  if (isdigit(c)) return c - '0';                 // 0 - 9
  else if (isxdigit(c)) return (c & 0xF) + 9;    // A-F, a-f
}
