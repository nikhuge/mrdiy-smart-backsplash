#include <pgmspace.h>

char main_js[] PROGMEM = R"=====(

var toHHMMSS = (secs) => {
    var sec_num = parseInt(secs, 10)
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60
    return [minutes,seconds]
        .map(v => v < 10 ? "0" + v : v)
        .join(":")
  }

var timer = 0;


function updateTimer() {
    timer = timer -1 ;
    if( timer > 0 ) document.getElementById("mrdiy_timer").innerHTML = toHHMMSS(timer);
    else document.getElementById("mrdiy_timer").innerHTML = toHHMMSS(0 );
}

   
window.addEventListener('DOMContentLoaded', (event) => {

   setInterval(updateTimer, 1000);
   setInterval(updateStatuses, 5000);
   getTimeLeft();
});

function updateStatuses(){
  
   getTimeLeft();
   getCookTopStatus();
   getCirculationStatus();
}

function getTimeLeft(){

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
   if (xhttp.readyState == 4 && xhttp.status == 200) {
     timer = xhttp.responseText;
   }
  };
  xhttp.open('GET', 'timer', true);
  xhttp.send();
}

function getCookTopStatus(){

  var xhttp_cooktop = new XMLHttpRequest();
  xhttp_cooktop.onreadystatechange = function() {
   if (xhttp_cooktop.readyState == 4 && xhttp_cooktop.status == 200) {
     if( xhttp_cooktop.responseText == "1" ) document.getElementById("ct_id").style.color = "#000000";
     else document.getElementById("ct_id").style.color = "#EEEEEE";
   }
  };
  xhttp_cooktop.open('GET', 'get_cooktop_status', true);
  xhttp_cooktop.send();
}

function getCirculationStatus(){

  var xhttp_circulation = new XMLHttpRequest();
  xhttp_circulation.onreadystatechange = function() {
   if (xhttp_circulation.readyState == 4 && xhttp_circulation.status == 200) {
     if( xhttp_circulation.responseText == "1" ) document.getElementById("cr_id").style.color = "#000000";
     else document.getElementById("cr_id").style.color = "#EEEEEE";
   }
  };
  xhttp_circulation.open('GET', 'get_circulation_status', true);
  xhttp_circulation.send();
}

function onAuto(event, dir) {
  event.preventDefault();
  submitVal('a', dir);
}

function submitVal(name, val) {
  var xhttp_submit = new XMLHttpRequest();
  xhttp_submit.open('GET', 'set?' + name + '=' + val, true);
  xhttp_submit.send();
}
)=====";
