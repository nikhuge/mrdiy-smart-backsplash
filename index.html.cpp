#include <pgmspace.h>
char index_html[] PROGMEM = R"=====(
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MrDIY Smart Backsplash</title>
    <link href='https://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>
<style>

html,body{height:100%;background-color:#eee;}
body{display:-ms-flexbox;display:flex;color:#fff;text-shadow:0 .05rem .1rem rgba(0, 0, 0, .5);box-shadow:inset 0 0 5rem rgba(0, 0, 0, .5);}
.cover-container{max-width:42em;}
.masthead{margin-bottom:2rem;}
.masthead-brand{margin-bottom:0;}
@media (min-width: 48em){
.masthead-brand{float:left;}
}
.cover{padding:0 1.5rem;}
.cover .btn-lg{padding:.75rem 1.25rem;font-weight:700;}
:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1ƒ;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;}
*,::after,::before{box-sizing:border-box;}
html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:transparent;}
footer,header,main{display:block;}
body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;}
h1,h3{margin-top:0;margin-bottom:.5rem;}
p{margin-top:0;margin-bottom:1rem;}
a{color:#007bff;text-decoration:none;background-color:transparent;}
a:hover{color:#0056b3;text-decoration:underline;}
h1,h3{margin-bottom:.5rem;font-weight:500;line-height:1.2;}
h1{font-size:2.5rem;}
h3{font-size:1.75rem;}
.lead{font-size:1.25rem;font-weight:300;}
.btn{display:inline-block;font-weight:400;color:#212529;text-align:center;vertical-align:middle;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;background-color:transparent;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;}
@media (prefers-reduced-motion:reduce){
.btn{transition:none;}
}
.btn:hover{color:#212529;text-decoration:none;}
.btn:focus{outline:0;box-shadow:0 0 0 .2rem rgba(0,123,255,.25);}
.btn:disabled{opacity:.65;}
.btn-success{color:#fff;background-color:#28a745;border-color:#28a745;}
.btn-success:hover{color:#fff;background-color:#218838;border-color:#1e7e34;}
.btn-success:focus{box-shadow:0 0 0 .2rem rgba(72,180,97,.5);}
.btn-success:disabled{color:#fff;background-color:#28a745;border-color:#28a745;}
.btn-warning{color:#212529;background-color:#ffc107;border-color:#ffc107;}
.btn-warning:hover{color:#212529;background-color:#e0a800;border-color:#d39e00;}
.btn-warning:focus{box-shadow:0 0 0 .2rem rgba(222,170,12,.5);}
.btn-warning:disabled{color:#212529;background-color:#ffc107;border-color:#ffc107;}
.btn-lg{padding:.5rem 1rem;font-size:1.25rem;line-height:1.5;border-radius:.3rem;}
.d-flex{display:-ms-flexbox!important;display:flex!important;}
.flex-column{-ms-flex-direction:column!important;flex-direction:column!important;}
.w-100{width:100%!important;}
.h-100{height:100%!important;}
.p-3{padding:1rem!important;}
.mt-auto{margin-top:auto!important;}
.mx-auto{margin-right:auto!important;}
.mb-auto{margin-bottom:auto!important;}
.mx-auto{margin-left:auto!important;}
.text-center{text-align:center!important;}
@media print{
*,::after,::before{text-shadow:none!important;box-shadow:none!important;}
h3,p{orphans:3;widows:3;}
h3{page-break-after:avoid;}
body{min-width:992px!important;}
}
</style>
  </head>
  <body class="text-center">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
    <div class="inner">
      <h3 id="ct_id" class="masthead-brand" style="color:#eee;float:left; width:50%;padding-left:5px;">COOKTOP</h3>
      <h3 id="cr_id" class="masthead-brand" style="color:#eee;float:right;width:50%;padding-right:5px;">CIRCULATION</h3>

    </div>
  </header>

  <main role="main" class="inner cover">
    <h1 id="mrdiy_timer" class="cover-heading"m style="font-size:90px;font-family: 'Orbitron', sans-serif;"></h1>
    <p class="lead"><br /></p>
    <p class="lead">
      <a href="/sub_a_minute" class="btn btn-lg btn-warning" style="font-size:50px;font-family: 'Orbitron', sans-serif;padding:5px 30px 10px 30px">-</a>
      <a href="/add_a_minute" class="btn btn-lg btn-success" style="font-size:50px;font-family: 'Orbitron', sans-serif;padding:5px 30px 10px 30px;margin-left:30px">+</a>
    </p>
  </main>

  <footer class="mastfoot mt-auto"></footer>
</div>
<script type='text/javascript' src='main.js'></script>
</body>
</html>

)=====";
