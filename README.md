# MrDIY Smart Backsplash

A wifi-connected smart backsplash with a timer, visual notifier, beeper and night light. It is controlled using a web interface, MQTT command messages and physical buttons. It is built around ESP-12F module featuring an ESP8266 microcontroller.
<br><br>

**Watch the video**: click on the image below:

[![How I embedded a Smart RGB LED Light Strip in my Kitchen Backsplash youtube video](https://img.youtube.com/vi/So64VUqMBR4/0.jpg)](https://www.youtube.com/watch?v=So64VUqMBR4)


<br><br>
## Thanks
Many thanks to all the authors and contributors to the open source libraries I have used. You have done an amazing job for the community!
